﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FridayPractice
{
    class Anniversary : Party
    {
        int readVenue;
        int readPackage;
        string bookingDate;
        int bookingId;
        public void AnniversaryOrganizing(int totalPeople, string date, int value)
        {
            try
            {
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("wElCoMe To AnNiVeRsArY BoOkInG SyStEm");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("wElCoMe To AnNiVeRsArY BoOkInG SyStEm");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                readPackage = Convert.ToInt32(Console.ReadLine());

                switch (readPackage)
                {
                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

            void SilverPackage()
            {
                string packageSilver = "Silver Package";
                base.decorationCharges = 15000;
                base.cateringCharges = 50000;
                DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);

            }
            void GoldPackage()
            {
                string packageGold = "Gold Package";
                base.decorationCharges = 20000;
                base.cateringCharges = 75000;
                DisplayPartyDetails(packageGold, base.decorationCharges, base.cateringCharges);
            }
            void PlatinumPackage()
            {
                string packagePlatinum = "Platinum Package";
                base.decorationCharges = 30000;
                base.cateringCharges = 100000;
                DisplayPartyDetails(packagePlatinum, base.decorationCharges, base.cateringCharges);
            }

        }
        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package" && noOfPeople <= 150)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe and Extra One Diffrent Snacks");
                Console.WriteLine("Maximum 150 Peoples Allowed");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package" && noOfPeople > 150 && noOfPeople <= 250)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails and Extra One Diffrent Snacks");
                Console.WriteLine("Gather upto 250 peoples");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("            INCLUDES       ");
                Console.WriteLine("Includes Unlimited Buffe   Mocktails   Cocktails and Extra Two Diffrent Snacks");
                Console.WriteLine("Free Parking Facility");
                Console.WriteLine("Gather upto 500 peoples");
                Console.WriteLine();
            }
        }
    }
}
