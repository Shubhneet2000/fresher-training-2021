﻿using CoreApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


namespace Core_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

 
   
    public class EmployeeController : Controller
    {
        private List<EmployeeModel> employeeList = new List<EmployeeModel>();

        public EmployeeController()
        {
            employeeList.Add(new EmployeeModel
            {
                EmployeeId = 21,
                EmployeeName = "Employee 01",
                EmployeeAge = 23
            });

            employeeList.Add(new EmployeeModel
            {
                EmployeeId = 22,
                EmployeeName = "Employee 02",
                EmployeeAge = 27
            });

            employeeList.Add(new EmployeeModel
            {
                EmployeeId = 23,
                EmployeeName = "Employee 03",
                EmployeeAge = 24
            });
        }

        [HttpGet]
        [ProducesResponseType(typeof(EmployeeModel), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(DBNull), (int)HttpStatusCode.NotFound)]
        public Task<EmployeeModel> GetEmployee()
        {
            var test = new EmployeeModel
            {
                EmployeeId = 31,
                EmployeeName = "Test 32",
                EmployeeAge = 43
            };

            return Task.FromResult(test);
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(EmployeeModel), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(DBNull), (int)HttpStatusCode.NotFound)]
        public Task<EmployeeModel> GetEmployee(int id)
        {
            var data = employeeList.Find(f => f.EmployeeId == id);

            return Task.FromResult(data);
        }


        [HttpGet]
        [Route("GetAll")]
        [ProducesResponseType(typeof(IEnumerable<EmployeeModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(DBNull), (int)HttpStatusCode.NotFound)]
        public Task<IEnumerable<EmployeeModel>> GetAllEmployee()
        {
            return Task.FromResult((IEnumerable<EmployeeModel>)employeeList);
        }

        [HttpPost]
        

        public IActionResult Index(Int32 EmployeeId, string EmployeeName)
        {
            ViewBag.EmployeeName = string.Format("Name: {0} {1}", EmployeeId, EmployeeName);
            return View();
        }


        [Route("api/v{version:apiVersion}/[controller]")]
        [ApiController]
        
        [ApiVersion("1.0")]
        
        public class DefaultController : ControllerBase
        {
            string[] authors = new string[]
            { "Joydip Kanjilal", "Steve Smith", "Stephen Jones" };
            [HttpGet]
            public IEnumerable<string> Get()
            {
                return authors;
            }
            [HttpGet("{id}")]
            [MapToApiVersion("2.0")]
            public string Get(int id)
            {
                return authors[id];
            }
        }

    }
}
