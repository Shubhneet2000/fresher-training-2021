﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebApplicationUsingUI.Models.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;


namespace WebApplicationUsingUI.Employee1.Controllers
{
    [Area("Employee")]
    public class EmployeeController : Controller
    {
        private readonly ILogger _logger;
        public EmployeeController(ILogger<EmployeeController> logger)
        {
            _logger = logger;
        }

        // GET: EmployeeController
        public ActionResult Index()
        {
            //_logger.LogInformation("Custom Information Log : from Employee Area");
            //_logger.LogWarning("Warning Log : from Employee Area");
            //_logger.LogError("Error Log : from Employee Area");
            //_logger.LogCritical("Critical Log : from Employee Area");
            return View("Employee", new EmployeeViewModel());
        }

        // GET: EmployeeController/Details/5
        public async Task<ActionResult> EmployeeAsync(EmployeeViewModel employee)
        {
            string baseURL = "https://localhost:44370/";
            List<EmployeeViewModel> dataType1 = new List<EmployeeViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Employee");

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    employee = JsonConvert.DeserializeObject<EmployeeViewModel>(result);
                }
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Employee/22");

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    employee = JsonConvert.DeserializeObject<EmployeeViewModel>(result);
                }
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Employee/GetAll");

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    dataType1 = JsonConvert.DeserializeObject<List<EmployeeViewModel>>(result);
                }
            }

            ViewBag.EmployeeId = employee.EmployeeId;
            ViewBag.EmployeeName = employee.EmployeeName;
            ViewBag.EmployeeAge = employee.EmployeeAge;

            return View("Employee", employee);
        }
    }
}
