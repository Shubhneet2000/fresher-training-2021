﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationUsingUI.Services
{
    public interface IMyLogger
    {
        void LogMessage();
        void LogMessage(string message);
    }
}
