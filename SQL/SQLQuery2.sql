                                                /* Group by  and having Concept */
Select * from Workers
Select DEPARTMENT from Workers group by DEPARTMENT
Select DEPARTMENT from Workers group by DEPARTMENT having  count(DEPARTMENT) >1
Select DEPARTMENT from Workers group by DEPARTMENT having  count(DEPARTMENT) >2
Select DEPARTMENT from Workers group by DEPARTMENT having  sum(SALARY) > 200000
Select FIRST_NAME from Workers group by FIRST_NAME having  sum(SALARY) > 200000

Select * from Workers
CREATE Trigger TriggerTest 
on Workers
FOR UPDATE 

AS BEGIN
print'Cannot insert'
rollback transaction
END
insert into Workers
values(9,'shivam','gupta',300000,NULL,'Account')

Update Workers
set FIRST_NAME = 'Mohan' where LAST_NAME = 'gupta' 




alter Trigger TriggerTest 
on Workers
 after delete
AS BEGIN
print'Deleted'
END
insert into Workers
values(10,'shiv','gupta',330000,NULL,'Account')

Delete Workers
from Workers where LAST_NAME = 'gupta'
Select * from Workers

alter Trigger TriggerTest 
on Workers
 after update
AS BEGIN
print'Value Updated'
END
insert into Workers
values(10,'Shubhneet','Tiwari',330000,NULL,'Account')

update Workers
set FIRST_NAME='Shubh' where LAST_NAME = 'Tiwari'
Select * from Workers