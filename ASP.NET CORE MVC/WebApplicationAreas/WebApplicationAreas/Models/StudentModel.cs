﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationAreas.Models
{
    public class StudentModel
    {

        public string Name { get; set; }
        public int Email { get; set; }
        public string Mobile { get; set; }
        public string Age { get; set; }
        public string Address { get; set; }
    }
}
