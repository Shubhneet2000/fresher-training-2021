﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateProgram
{
   public class Operations
    {
        public delegate void delmethod();

        public class ProgramDel
        {

            public static void display()
            {
                Console.WriteLine("Hello!");
            }

            public static void show()
            {
                Console.WriteLine("Hi!");
            }

            public void print()
            {
                Console.WriteLine("Print");
            }

        }

    }
}


