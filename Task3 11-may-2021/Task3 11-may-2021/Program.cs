﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_11_may_2021
{
   
    public interface IPaiter
    {
        void Paint();
    }
    public  interface IPaiter1: IPaiter
    {
        void Paint();
    }
    public class Vehicle : IPaiter1
    {
        public enum wheels{ Car = 4, Bike = 2 }

        public void Paint()
        {
            Vehicle sample = new Vehicle();

            sample.Paint();

        }
    }

}
