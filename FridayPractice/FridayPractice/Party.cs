﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FridayPractice
{

    public class Party
    {
        public string bookingType;
        public int noOfPeople;
        public string partyVenue;
        public double decorationCharges;
        public double cateringCharges;

        public virtual void PartyPackage(string partyVenue)
        {

        }

        public virtual void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {

        }

    }
}
