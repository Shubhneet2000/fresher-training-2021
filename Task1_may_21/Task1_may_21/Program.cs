﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_may_21
{
    class Person
    {
        public int age;
        public string name;
        public void SetAge(int n)
        {
            age = n;
        }
        public void SetName(string b)
        {
            name = b;
        }
        
        public void Greet()
        {
            Console.WriteLine(name + "Say hello");
        }

    }
    class Student : Person
    {
        
        public void GoToClasses()
        {
            Console.WriteLine("I’m going to class.");
        }
        public void SetName(string b)
        {
            name = b;
        }
        public void ShowAge()
        {
            Console.WriteLine("My age is: " + age);
        }
        public void Greet()
        {
            Console.WriteLine(name + "Say hello");
        }
    }
    class teacher : Person
    {
        
        public void Explain()
        {
            string subject;
            Console.WriteLine("Explanation begins");
        }
        public void SetName(string b)
        {
            name = b;
        }
        public void ShowAge()
        {
            Console.WriteLine("My age is: " + age);
        }
        public void Greet()
        {
            Console.WriteLine(name + "Says hello");
        }

    }
    class StudentAndTeacherTest
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.SetName("Shubhneet ");
            p.Greet();

            Student s = new Student();
            s.SetName("Ram ");
            s.SetAge(21);
            s.Greet();
            s.ShowAge();
            teacher t = new teacher();
            t.SetName("Vishal ");
            t.SetAge(30);
            t.Greet();
            t.Explain();
            Console.ReadLine();
        }
    }
}

