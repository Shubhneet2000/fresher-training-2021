﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqProgramPractice
{
    class Program
    {
		public static void Main()
		{
			IList<Student> studentList1 = new List<Student>() {
			new Student() { StudentID = 1, StudentName = "Shubhneet", Age = 18 } ,
			new Student() { StudentID = 2, StudentName = "Shivam",  Age = 18 } ,
			new Student() { StudentID = 3, StudentName = "Rakesh",  Age = 25 } ,
			new Student() { StudentID = 5, StudentName = "Ronak" , Age = 19 },
			new Student() { StudentID = 5, StudentName = "Shivam" , Age = 21 },
			new Student() { StudentID = 5, StudentName = "mikku" , Age = 22 },
			new Student() { StudentID = 5, StudentName = "motu" , Age = 08 }
		};

			IList<Student> studentList2 = new List<Student>() {
				new Student() { StudentID = 3, StudentName = "Shubhneet",  Age = 25 } ,
				new Student() { StudentID = 5, StudentName = "piku" , Age = 19 } ,
				new Student() { StudentID = 6, StudentName = "Ram" , Age = 29 },
				new Student() { StudentID = 5, StudentName = "piku" , Age = 21 },
			    new Student() { StudentID = 5, StudentName = "mikku" , Age = 22 },
			    new Student() { StudentID = 5, StudentName = "motu" , Age = 08 }
			};

			var resultedCol = studentList1.Intersect(studentList2, new StudentComparer());

			foreach (Student std in resultedCol)
				Console.WriteLine(std.StudentName);
			IList<int> collection1 = new List<int>() { 1, 2, 3,4,5,6,7,8,9 };
			IList<int> collection2 = new List<int>() { 10, 11, 6,12,13,14,15,16,17 };

			var collection3 = collection1.Concat(collection2);
			Console.WriteLine("Concat the list");
			foreach (int i in collection3)
				Console.Write(" "+i);
			var studentName = studentList1.Concat(studentList2);
			Console.WriteLine("");
			Console.WriteLine("Concat the Student Age:");
			foreach (Student stdd in studentName)
				Console.Write(" "+stdd.Age);
			Console.WriteLine("");
			Console.WriteLine("Concat the Student Name:");
			foreach (Student stdd in studentName)
				Console.Write("{0}, ",stdd.StudentName);


			var distinctList1 = studentList1.Distinct(new StudentComparerList());
			Console.WriteLine(" ");
			Console.WriteLine("Distinct Student Name in List1:");
			foreach (var str in distinctList1)
				Console.WriteLine(str.StudentName);


			var result = studentList1.Except(studentList2, new StudentComparer());
			Console.WriteLine("Student List1 Except the Student List2:");
			foreach (var str in result)
				Console.WriteLine(str.Age);

			Console.ReadKey();
		}
	}
}
