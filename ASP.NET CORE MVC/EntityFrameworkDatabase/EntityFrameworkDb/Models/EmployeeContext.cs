﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace EntityFrameworkDb.Models
{
    public class EmployeeContext : DbContext
    {
        public EmployeeContext(DbContextOptions<EmployeeContext> options) : base(options)
        {

        }
        public Microsoft.EntityFrameworkCore.DbSet<tblSkill> tblSkills { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<tblEmployee> tblEmployees { get; set; }
    }
}
