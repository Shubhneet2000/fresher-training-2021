﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationUsingUI.Models.Employee;
using WebApplicationUsingUI.Services;

namespace WebApplicationUsingUI.Controllers
{
    public class EmployeeController : Controller
    {
        
        private readonly ILogger _logger;
        private readonly IMyLogger _mylogger;
        public EmployeeController(ILogger<EmployeeController> logger, IMyLogger mylogger)
        {
            _logger = logger;
            _mylogger = mylogger;
        }

        // GET: EmployeeController
        public ActionResult Index()
        {
            _mylogger.LogMessage();
            return View("Employee", new EmployeeViewModel());
        }

        // GET: EmployeeController/Details/5
        public ActionResult Employee(EmployeeViewModel employee)
        {
            ViewBag.EmployeeId = employee.EmployeeId;
            ViewBag.EmployeeName = employee.EmployeeName;
            ViewBag.EmployeeAge = employee.EmployeeAge;

            return View("Employee", employee);
        }


        
    }
}
