﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationUsingUI.Services
{
    public class MyLogger : IMyLogger
    {
        public void LogMessage()
        {
            Debug.Write(string.Format("Message from Custom Logger !"));
        }

        public void LogMessage(string message)
        {
            Debug.Write(string.Format("Message from Custom Logger ! + : " + message));
        }
    }
}
