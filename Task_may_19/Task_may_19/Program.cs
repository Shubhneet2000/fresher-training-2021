﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_may_19
{
    public abstract class Shapes
    {
        public virtual void Calculation();
    }
    public  class Sphere:Shapes
    {
        
       public double volume, surface_area,perimeter,Area, circumference;
       public double r,s,h,l,b;
        public double PI = 3.14;
        public override void Calculation()
        {
            try
            {
                Console.WriteLine("Enter the radius of Sphere :");
                double r = Convert.ToDouble(Console.ReadLine());
                volume = (4.0 / 3) * PI * r * r * r;
                surface_area = 4 * PI * r * r;
                Console.WriteLine("Volume of sphere is : " + volume);

                Console.WriteLine("Surface area of sphere is : " + surface_area);
            }
            catch(Exception)
            {
                Console.WriteLine("Do not Enter Null Value ! Please try Again");
                Console.WriteLine("Enter the radius of Sphere :");
                double r = Convert.ToDouble(Console.ReadLine());
                volume = (4.0 / 3) * PI * r * r * r;
                surface_area = 4 * PI * r * r;
                Console.WriteLine("Volume of sphere is : " + volume);

                Console.WriteLine("Surface area of sphere is : " + surface_area);
            }
        }
    }
    public class Cube:Sphere
    {
        public override void Calculation()
        {
            try
            {
                Console.WriteLine("Enter the side of Sphere :");
                double s = Convert.ToDouble(Console.ReadLine());
                volume = s * s * s;
                surface_area = 6 * s * s;
                Console.WriteLine("Volume of cube is : " + volume);
                Console.WriteLine("Surface area of cube is : " + surface_area);
            }
            catch(Exception)
            {
                Console.WriteLine("Do not Enter Null Value ! Please try Again");
                Console.WriteLine("Enter the side of Sphere :");
                double s = Convert.ToDouble(Console.ReadLine());
                volume = s * s * s;
                surface_area = 6 * s * s;
                Console.WriteLine("Volume of cube is : " + volume);
                Console.WriteLine("Surface area of cube is : " + surface_area);
            }
        }
    }
    public class Cylinder:Cube
    {
        public override void Calculation()
        {
            try
            {
                Console.WriteLine("Enter the radius of Sphere :");
                double r = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the height of Sphere :");
                double h = Convert.ToDouble(Console.ReadLine());
                volume = PI * r * r * h;
                Console.WriteLine("Volume of cylinder is : " + volume);
                surface_area = 2 * PI * r * (r + h);
                Console.WriteLine("Surface area of cylinder is : " + surface_area);
            }
            catch(Exception)
            {
                Console.WriteLine("Do not Enter Null Value ! Please try Again");
                Console.WriteLine("Enter the radius of Sphere :");
                double r = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the height of Sphere :");
                double h = Convert.ToDouble(Console.ReadLine());
                volume = PI * r * r * h;
                Console.WriteLine("Volume of cylinder is : " + volume);
                surface_area = 2 * PI * r * (r + h);
                Console.WriteLine("Surface area of cylinder is : " + surface_area);
            }
        }
    }
    public class Circle:Cylinder
    {
        public override void Calculation()
        {
            try
            {
                Console.WriteLine("Enter the radius of Circle :");
                double r = Convert.ToDouble(Console.ReadLine());
                Area = PI * r * r;
                circumference = 2 * PI * r;
                Console.WriteLine("Area of Circle is : " + Area);
                Console.WriteLine("Circumference of Circle is : " + circumference);
            }
            catch(Exception)
            {
                Console.WriteLine("Do not Enter Null Value ! Please try Again");
                Console.WriteLine("Enter the radius of Circle :");
                double r = Convert.ToDouble(Console.ReadLine());
                Area = PI * r * r;
                circumference = 2 * PI * r;
                Console.WriteLine("Area of Circle is : " + Area);
                Console.WriteLine("Circumference of Circle is : " + circumference);
            }

        }
    }
    public class Rectangle:Circle
    {
        public override void Calculation()
        {
            try
            {
                Console.WriteLine("Enter the length of Rectangle :");
                double l = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the breadth of Rectangle :");
                double b = Convert.ToDouble(Console.ReadLine());
                Area = l * b;
                perimeter = 2 * l + 2 * b;
                Console.WriteLine("Area of Rectangle is : " + Area);
                Console.WriteLine("Perimeter of Rectangle is : " + perimeter);
            }
            catch(Exception)
            {
                Console.WriteLine("Do not Enter Null Value ! Please try Again");
                Console.WriteLine("Enter the length of Rectangle :");
                double l = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the breadth of Rectangle :");
                double b = Convert.ToDouble(Console.ReadLine());
                Area = l * b;
                perimeter = 2 * l + 2 * b;
                Console.WriteLine("Area of Rectangle is : " + Area);
                Console.WriteLine("Perimeter of Rectangle is : " + perimeter);
            }
            
        }
    }
   public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Sphere obj1 = new Sphere();
                Cube obj2 = new Cube();
                Cylinder obj3 = new Cylinder();
                Circle obj4 = new Circle();
                Rectangle obj5 = new Rectangle();
                string condition;
                do
                {

                    Console.WriteLine("Select Shape");
                    Console.WriteLine("1. Sphere");
                    Console.WriteLine("2. Cube");
                    Console.WriteLine("3. Cylinder");
                    Console.WriteLine("4. Cylinder");
                    Console.WriteLine("5. Cylinder");

                    double value1 = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine();



                    switch (value1)
                    {
                        case 1:
                            try
                            {

                                obj1.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj1.Calculation();
                            }
                            break;
                        case 2:
                            try
                            {

                                obj2.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj2.Calculation();

                            }
                            break;
                        case 3:
                            try
                            {

                                obj3.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj3.Calculation();

                            }

                            break;
                        case 4:
                            try
                            {

                                obj4.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj4.Calculation();

                            }

                            break;
                        case 5:
                            try
                            {

                                obj5.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj5.Calculation();

                            }

                            break;
                        default:
                            Console.WriteLine("Entered Wrong Choice ! Enter [yes/Yes] to Select choice again :");
                            // condition = Console.ReadLine();

                            break;

                    }
                    Console.WriteLine("Do You Want select shapes Again...");
                    condition = Console.ReadLine();

                } while (condition == "yes" || condition == "Yes");
                Console.ReadLine();
            }
            catch(Exception)
            {
                Console.WriteLine("Do not Enter Null Value ! Please try Again");
                Sphere obj1 = new Sphere();
                Cube obj2 = new Cube();
                Cylinder obj3 = new Cylinder();
                Circle obj4 = new Circle();
                Rectangle obj5 = new Rectangle();
                string condition;
                do
                {

                    Console.WriteLine("Select Shape");
                    Console.WriteLine("1. Sphere");
                    Console.WriteLine("2. Cube");
                    Console.WriteLine("3. Cylinder");
                    Console.WriteLine("4. Cylinder");
                    Console.WriteLine("5. Cylinder");

                    double value1 = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine();



                    switch (value1)
                    {
                        case 1:
                            try
                            {

                                obj1.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj1.Calculation();
                            }
                            break;
                        case 2:
                            try
                            {

                                obj2.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj2.Calculation();

                            }
                            break;
                        case 3:
                            try
                            {

                                obj3.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj3.Calculation();

                            }

                            break;
                        case 4:
                            try
                            {

                                obj4.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj4.Calculation();

                            }

                            break;
                        case 5:
                            try
                            {

                                obj5.Calculation();

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                obj5.Calculation();

                            }

                            break;
                        default:
                            Console.WriteLine("Entered Wrong Choice ! Enter [yes/Yes] to Select choice again :");
                            // condition = Console.ReadLine();

                            break;

                    }
                    Console.WriteLine("Do You Want select shapes Again...");
                    condition = Console.ReadLine();

                } while (condition == "yes" || condition == "Yes");
                Console.ReadLine();
            }
        }
    }


}
