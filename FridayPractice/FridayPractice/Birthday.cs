﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FridayPractice
{

    class Birthday : Party
    {
        int readVenue;
        int readPackage;
        string bookingDate;
        int bookingId;
        public void BirthdayOrganizing(int totalPeople, string date, int value)
        {
            try
            {
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Birthday Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                bookingDate = date;
                bookingId = value;
                base.noOfPeople = totalPeople;
                Console.WriteLine("Welcome To Birthday Booking System");
                Console.WriteLine("--- Venue Available : ---");
                Console.WriteLine("1. The Oberoi, New Delhi");
                Console.WriteLine("2. ITC Maurya, A Luxury Collection Hotel, New Delhi");
                readVenue = Convert.ToInt32(Console.ReadLine());
                switch (readVenue)
                {
                    case 1:
                        base.partyVenue = "Reddisson Blue- Dwarka";
                        PartyPackage(base.partyVenue);
                        break;
                    case 2:
                        base.partyVenue = "HolidayInn Hotel- Gurgaon";
                        PartyPackage(base.partyVenue);
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                        break;
                }
            }

        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                string checkCondition = "n";
                do
                {
                    Console.WriteLine("Select Packages : ");
                    Console.WriteLine("1. Silver Package ");
                    Console.WriteLine("2. Gold Package : ");
                    Console.WriteLine("3. Platinum Package : ");
                    readPackage = Convert.ToInt32(Console.ReadLine());

                    switch (readPackage)
                    {

                        case 1:
                            SilverPackage();
                            break;
                        case 2:
                            GoldPackage();
                            break;
                        case 3:
                            PlatinumPackage();
                            break;
                        default:
                            Console.WriteLine("Entered Wrong Choice ! Enter [y/Y] to Select choice again :");
                            checkCondition = Console.ReadLine();
                            break;
                    }
                } while (checkCondition == "y" || checkCondition == "Y");
            }
            catch (Exception e)
            {
                string checkCondition = "n";
                do
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Select Packages : ");
                    Console.WriteLine("1. Silver Package ");
                    Console.WriteLine("2. Gold Package : ");
                    Console.WriteLine("3. Platinum Package : ");
                    readPackage = Convert.ToInt32(Console.ReadLine());

                    switch (readPackage)
                    {

                        case 1:
                            SilverPackage();
                            break;
                        case 2:
                            GoldPackage();
                            break;
                        case 3:
                            PlatinumPackage();
                            break;
                        default:
                            Console.WriteLine("Entered Wrong Choice ! Enter [y/Y] to Select choice again :");
                            checkCondition = Console.ReadLine();
                            break;
                    }
                } while (checkCondition == "y" || checkCondition == "Y");
            }
            void SilverPackage()
            {
                string packageSilver = "Silver Package";
                base.decorationCharges = 10000;
                base.cateringCharges = 20000;
                DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);

            }
            void GoldPackage()
            {
                string packageGold = "Gold Package";
                base.decorationCharges = 12000;
                base.cateringCharges = 25000;
                DisplayPartyDetails(packageGold, base.decorationCharges, base.cateringCharges);
            }
            void PlatinumPackage()
            {
                string packagePlatinum = "Platinum Package";
                base.decorationCharges = 15000;
                base.cateringCharges = 30000;
                DisplayPartyDetails(packagePlatinum, base.decorationCharges, base.cateringCharges);
            }



        }
        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package")
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("Includes Unlimited Buffe and Extra One Diffrent Snacks");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package")
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("Includes Unlimited Buffe   Mocktails and Extra One Diffrent Snacks");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + bookingId);
                Console.WriteLine("Package : " + packageName);
                Console.WriteLine("Venue : " + base.partyVenue);
                Console.WriteLine("Booking Date : " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :" + totalCost / base.noOfPeople);
                Console.WriteLine("Includes Unlimited Buffe   Mocktails   Cocktails and Extra Two Diffrent Snacks");
                Console.WriteLine("Free Goodies");
            }
        }
    }
}
