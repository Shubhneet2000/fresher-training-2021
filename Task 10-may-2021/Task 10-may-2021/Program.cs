﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_may_10_2021
{
    class Vehicle
    {
        public int max_speed = 200;

    }
    class Car : Vehicle
    {
        public string color;
        public int no_of_wheels = 4;
        public int price_of_car;
        public void calculate_toll_amount()
        {
            Console.WriteLine("Color of car :" + color);
            Console.WriteLine("Wheels of Car : " + no_of_wheels);
            Console.WriteLine("Price of Car :" + price_of_car);
            Console.WriteLine("Max speed of Car :" + max_speed);


        }
    }
    class Bike : Car
    {
        public string color_of_bike;
        public int no_of_wheels_of_bike = 2;
        public int price_of_bike;
        public void calculate_toll_amount_of_bike()
        {
            Console.WriteLine("Color of bike :" + color_of_bike);
            Console.WriteLine("Wheels of bike : " + no_of_wheels_of_bike);
            Console.WriteLine("Price of bike :" + price_of_bike);
            Console.WriteLine("Max speed of bike :" + max_speed);

        }
        public static void Main(string[] args)
        {
            Bike obj = new Bike();
            obj.color = "Red";
            obj.price_of_car = 5000000;
            obj.color_of_bike = "Blue";
            obj.price_of_bike = 100000;
            obj.calculate_toll_amount();
            obj.calculate_toll_amount_of_bike();
            Console.ReadKey();

        }

    }

}
