﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11_may_2021
{
    class Program
    {

        public enum Months { January = 1, February, March, April, May, June, July, August, September, October, November, December }
        
        public static void Main(string[] args)
        {
            Dictionary<int, string> Days = new Dictionary<int, string>();
            Days.Add(1, "31 Days");
            Days.Add(2, "28 Days");
            Days.Add(3, "31 Days");
            Days.Add(4, "30 Days");
            Days.Add(5, "31 Days");
            Days.Add(6, "30 Days");
            Days.Add(7, "31 Days");
            Days.Add(8, "31 Days");
            Days.Add(9, "30 Days");
            Days.Add(10, "31 Days");
            Days.Add(11, "30 Days");
            Days.Add(12, "31 Days");
            
            int x = (int)Months.August;
            var myKey = Days.FirstOrDefault(z => z.Key == x).Value;
            Console.WriteLine(""+ myKey);
            Console.ReadLine();

        }

    }
}
