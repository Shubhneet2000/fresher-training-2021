﻿using CalculatorApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorApi.Controllers
{
    public class CalculatorController : Controller
    {
        private readonly ILogger<CalculatorController> _logger;

        public CalculatorController(ILogger<CalculatorController> logger)
        {
            _logger = logger;
        }

        public IActionResult Calculator()
        {
            TempData["Key"] = "data from index method";
            return View("Calculator",new CalculatorViewModel());
        }

        [HttpPost]
        public IActionResult Calculator(CalculatorViewModel c, string calculate)
        {
            
            if(calculate=="add")
            {
                c.Result = c.FirstNo + c.SecondNo;
            }
            else if (calculate == "sub")
            {
                c.Result = c.FirstNo - c.SecondNo;
            }
            else if (calculate == "mul")
            {
                c.Result = c.FirstNo * c.SecondNo;
            }


            else
            {
                c.Result = c.FirstNo / c.SecondNo;
            }



            return View("Calculator", c);
        }

        public IActionResult site()
        {
            return View();
        }





    }
}
