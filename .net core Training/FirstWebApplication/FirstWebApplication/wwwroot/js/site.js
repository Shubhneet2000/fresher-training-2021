﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function Validate() {
	var firstname = document.form.firstname.value;
	var lastname = document.form.lastname.value;
	var email = document.form.emaill.value;
	var password = document.form.password.value;
	var cpassword = document.form.cpassword.value;
	var mobile = document.form.mobile.value;
	var address = document.form.address.value;



	//email id expression code
	var pwd_expression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/;
	var letters = /^[A-Za-z]+$/;
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var add = /^[a-zA-Z0-9\s,. '-]{3,}$/;
	var mob = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;


	if (firstname == '') {
		//alert('Please enter your name');
		document.getElementById('username').innerHTML = "Please enter your name";
		return false;
	}
	else if (!letters.test(firstname)) {
		//alert('Name field required only alphabet characters');
		document.getElementById('username').innerHTML = "Name field required only alphabet characters";
		return false;
	}
	else if (lastname == '') {
		//alert('Please enter your last name');
		document.getElementById('lName').innerHTML = "Please enter your last name";
		return false;
	}
	else if (!letters.test(lastname)) {
		//alert('Name field required only alphabet characters');
		document.getElementById('lName').innerHTML = "Name field required only alphabet characters";
		return false;
	}
	else if (email == '') {
		//alert('Please enter your email id');
		document.getElementById('emaill').innerHTML = "Please enter your email id";
		return false;
	}
	else if (!filter.test(email)) {
		//alert('Invalid email');
		document.getElementById('emall').innerHTML = "please fill";
		return false;
	}
	else if (password == '') {
		//alert('Please enter Password');
		document.getElementById('pass').innerHTML = "Please enter Password";
		return false;
	}
	else if (cpassword == '') {
		//alert('Please Enter Confirm Password');
		document.getElementById('cPass').innerHTML = "Please Enter Confirm Password";
		return false;
	}
	else if (!pwd_expression.test(password)) {
		//alert('Upper case, Lower case, Special character and Numeric letter are required in Password filed');
		document.getElementById('pass').innerHTML = "Upper case, Lower case, Special character and Numeric letter are required in Password filed";
		return false;
	}
	else if (password != cpassword) {
		//alert('Password not Matched');
		document.getElementById('cPass').innerHTML = "Password not Matched";
		return false;
	}
	else if (mobile == '') {
		//alert('Please enter mobile number');
		document.getElementById('tel').innerHTML = "Please enter mobile number";
		return false;
	}
	else if (!mob.test(mobile)) {
		//alert('follow the pattern 123-456-7891');
		document.getElementById('tel').innerHTML = "follow the pattern 123-456-7891";
		return false;
	}
	else if (address == '') {
		//alert('Please enter your address');
		document.getElementById('addr').innerHTML = "Please enter your address";
		return false;
	}
	else if (!add.test(address)) {
		//alert('Enter your full address');
		document.getElementById('addr').innerHTML = "Enter your full address";
		return false;
	}
	else if (document.form.password.valuelength < 6) {
		//alert('Password minimum length is 6');
		document.getElementById('pass').innerHTML = "Password minimum length is 6";
		return false;
	}
	else if (document.form.password.value.length > 12) {
		//alert('Password maximum length is 12');
		document.getElementById('pass').innerHTML = "Password maximum length is 12');";
		return false;
	}
	this.add();

	//else {
	//alert('Registration Succesful ');
	//}
	return false;
}
function add() {
	var firstname = document.form.firstname.value;
	var Mname = document.form.Mname.value;
	var lastname = document.form.lastname.value;
	var email = document.form.emaill.value;
	var mobile = document.form.mobile.value;
	var address = document.form.address.value;
	var Education = document.form.Education.value;
	var gender = document.form.gender.value;
	//var obj = { "uName1": firstname,"mName1":Mname, "lname1": lastname,"id1": emaill,"number1": mobile, "address1": address, "Education1":Education,"gender1":gender};

	var table = document.getElementById("myTable");
	//var tableLength = table.rows.length;
	var row = table.insertRow(1);

	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
	var cell6 = row.insertCell(5);
	var cell7 = row.insertCell(6);
	var cell8 = row.insertCell(7);


	cell1.innerHTML = firstname;
	cell2.innerHTML = Mname;
	cell3.innerHTML = lastname;
	cell4.innerHTML = email;
	cell5.innerHTML = mobile;
	cell6.innerHTML = address;
	cell7.innerHTML = Education;
	cell8.innerHTML = gender;




	//for (var data in obj) {
	//	var row = table.insertRow(tableLength).outerHTML = "<tr id='row1'><td>data</td> </tr>";
	//}
}
function myDeleteFunction() {
	document.getElementById("myTable").deleteRow(1);
}
function myFunction() {
	document.getElementById("myForm").reset();
}

