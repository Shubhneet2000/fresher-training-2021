﻿using WebApplicationAreas.Areas.Student.Filters;
using WebApplicationAreas.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace WebApplicationAreas.Areas.Student.Controllers
{
    [Area("Student")]
    //[ResultFilter]
    public class StudentController : Controller
    {
        private readonly ILogger<StudentController> _logger;

        public StudentController(ILogger<StudentController> logger)
        {
            _logger = logger;
        }

        [ServiceFilter(typeof(ResultFilter))]
        public IActionResult Index()
        {
            return View("Student", new StudentModel());
        }
        
        public IActionResult Student(StudentModel studentmodel)
        {
            ViewBag.Name = studentmodel.Name;
            ViewBag.Email = studentmodel.Email;
            ViewBag.Mobile = studentmodel.Mobile;
            ViewBag.Age = studentmodel.Age;
            ViewBag.Address = studentmodel.Address;

            return View("Student", studentmodel);
        }
    }
}

