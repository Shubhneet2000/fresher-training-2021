﻿using System.Collections.Generic;
using RegistrationForm.Models;

namespace RegistrationForm.Repositories
{
    public partial class IStudentRepository
    {
        public interface IStudentRepository
        {
            public List<Student> GetStudents();

        }
    }
}
