﻿using System;

namespace Core_API.Controllers
{
    internal class ApiVersionAttribute : Attribute
    {
        public ApiVersionAttribute(string v)
        {
            V = v;
        }

        public string V { get; }
    }
}