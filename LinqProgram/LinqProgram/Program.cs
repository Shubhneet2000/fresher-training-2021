﻿using LinqProgram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqProgram 
{
	
	public class Program
	{
		public static void Main()
		{
			// Student collection
			IList<Student> studentList = new List<Student>() {
				new Student() { StudentID = 1, StudentName = "Shiv", Age = 13, StandardID =1} ,
				new Student() { StudentID = 2, StudentName = "Ram",  Age = 21, StandardID =2 } ,
				new Student() { StudentID = 3, StudentName = "Bill",  Age = 18 , StandardID =1} ,
				new Student() { StudentID = 4, StudentName = "Raman" , Age = 20, StandardID =2} ,
				new Student() { StudentID = 5, StudentName = "Rohan" , Age = 15,StandardID =3 }
			};
			IList<Standard> standardList = new List<Standard>() {
	new Standard(){ StandardID = 1, StandardName="Standard 1"},
	new Standard(){ StandardID = 2, StandardName="Standard 2"},
	new Standard(){ StandardID = 3, StandardName="Standard 3"}
};
			IList<Student> studentList2 = new List<Student>() {
				new Student() { StudentID = 3, StudentName = "MishraJi",  Age = 25 } ,
				new Student() { StudentID = 5, StudentName = "TiwariJi" , Age = 19 }
			};


			// LINQ Query Method 
			var teenAgerStudent = studentList.Where(s => s.Age > 12 && s.Age < 20);

			Console.WriteLine("Name of Students who's age in between 12 to 20:");

			foreach (Student std in teenAgerStudent)
			{
				Console.WriteLine(std.StudentName);
			}
			var sumOfAge = studentList.Sum(s => s.Age);

			Console.WriteLine("Sum of all student's age: {0}", sumOfAge);

			var totalAdults = studentList.Sum(s => {

				if (s.Age >= 18)
					return 1;
				else
					return 0;
			});
			Console.WriteLine("Total Adult Students: {0}", totalAdults);
			var oldest = studentList.Max(s => s.Age);

			Console.WriteLine("Oldest Student Age: {0}", oldest);
			var groupJoin = standardList.GroupJoin(studentList,  //inner sequence
								std => std.StandardID, //outerKeySelector 
								s => s.StandardID,     //innerKeySelector
								(std, studentsGroup) => new // resultSelector 
								{
									Students = studentsGroup,
									StandarFulldName = std.StandardName
								});

			foreach (var item in groupJoin)
			{
				Console.WriteLine(item.StandarFulldName);

				foreach (var stud in item.Students)
					Console.WriteLine(stud.StudentName);
			}
			var orderByResult = from s in studentList
								orderby s.StudentName //Sorts the studentList collection in ascending order
								select s;

			var orderByDescendingResult = from s in studentList //Sorts the studentList collection in descending order
										  orderby s.StudentName descending
										  select s;

			Console.WriteLine("Ascending Order:");

			foreach (var std in orderByResult)
				Console.WriteLine(std.StudentName);

			Console.WriteLine("Descending Order:");

			foreach (var std in orderByDescendingResult)
				Console.WriteLine(std.StudentName);
			var avgAge = studentList.Average(s => s.Age);

			Console.WriteLine("Average Age of Student: {0}", avgAge);
			var result = studentList.Union(studentList2, new StudentComparer());
			Console.WriteLine("Union of StudentList and StudentList2:");
			foreach (var std in result)
				Console.WriteLine(std.StudentName);
			Console.ReadKey();
		}
	}

	
}
