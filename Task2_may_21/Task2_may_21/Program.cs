﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_may_21
{
	struct book
	{
		public string title;
		public string author;
	}
	public class strucExer9
	{
		public static void Main()
		{
			int nobook = 1000;
			book[] books = new book[nobook];
			int i, j, k = 1;
			Console.Write("\n\n Number of Books :\n");
			int n = int.Parse(Console.ReadLine());
			Console.Write("\n\nInsert the information of books :\n");
			
			for (j = 0; j < n; j++)
			{
				Console.WriteLine("Information of book {0} :", k);

				Console.Write("Input name of the book : ");
				books[j].title = Console.ReadLine();

				Console.Write("Input the author : ");
				books[j].author = Console.ReadLine();
				k++;
				Console.WriteLine();
			}

			for (i = 0; i < n; i++)
			{
				Console.WriteLine("{0}: Title = {1},  Author = {2}", i + 1, books[i].title, books[i].author);
				Console.WriteLine();
				
			}
			Console.ReadKey();

		}
	}

}
