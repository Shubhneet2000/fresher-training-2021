﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationFormTagHelper.Models
{
    public class Model
    {
        public int Name{ get; set; }
        public int Email { get; set; }
        public int Branch { get; set; }
        public int Gender { get; set; }
        public int Address { get; set; }
    }
}
