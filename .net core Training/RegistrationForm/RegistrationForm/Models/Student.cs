﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationForm.Models
{
    public class Student
    {
        internal string name;

        [Required(ErrorMessage = "First Name is required.")]
        public string fName { get; set; }
        public string mName { get; set; }
        [Required(ErrorMessage = "Last Name is required.")]
        public string lName { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        public string email { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        public string password { get; set; }
        public string mobile { get; set; }
        public string address { get; set; }
        public string education { get; set; }
        public string gender { get; set; }
    }
    
}
