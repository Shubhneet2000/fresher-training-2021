﻿using RegistrationForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationForm.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        public List<Student> _studentList;

        public StudentRepository()
        {
            _studentList = new List<Student>() {
            new Student() { fName = "shubh", mName = "kumar", lName = "Tiwari",  email = "shubhneet@gmail.com", password="Shubh@123",mobile="9876567894",address="Delhi",education="Graduation", gender = "male"},
            new Student() { fName = "shubh", mName = "kumar", lName = "Tiwari",  email = "shubhneet@gmail.com", password="Shubh@123",mobile="9876567894",address="Delhi",education="Graduation", gender = "male"}
                
        };
        }

        public new List<Student> GetStudents()
        {
            return _studentList;
        }
    }
}
