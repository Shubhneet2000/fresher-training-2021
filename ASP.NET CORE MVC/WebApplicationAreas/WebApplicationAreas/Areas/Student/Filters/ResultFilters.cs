﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationAreas.Areas.Student.Filters
{
    public class ResultFilter : IResultFilter
    {
        public void OnResultExecuted(ResultExecutedContext context)
        {
            var routdata = context.RouteData;
            var action = routdata.Values["action"];
            var controller = routdata.Values["controller"];
            Debug.Write(string.Format("Controller: {0} Action: {1} ", controller, action), "Log from Result Filter OnResultExecuted: ");
        }

        public void OnResultExecuting(ResultExecutingContext context)
        {
            var routdata = context.RouteData;
            var action = routdata.Values["action"];
            var controller = routdata.Values["controller"];
            Debug.Write(string.Format("Controller: {0} Action: {1} ", controller, action), "Log from Result Filter OnResultExecuting: ");
        }
    }
}
