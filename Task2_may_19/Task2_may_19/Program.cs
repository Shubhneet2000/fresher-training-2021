﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_may_19
{
    public class MusicalComposition
    {
        public string title;
        public String composer;
        public int year;
        public virtual void Display() 
        {
            Console.WriteLine("Title is : "  +title);
            Console.WriteLine("Composer is : "  +composer);
            Console.WriteLine("Year is : "  +year);

        }

        public MusicalComposition(string title, string composer, int year)
        {

    
            this.title = title;
            this.composer = composer;
            this.year = year;
    
        }
    }
    public class NationAnthem : MusicalComposition
    {
        string anthem;

        public NationAnthem(string title, string composer, int year, string anthem) : base(title, composer, year)
        {
            this.anthem = anthem;
        }
        public override void Display()
        {
    Console.WriteLine("Title is : "  +title);
    Console.WriteLine("Composer is : " + composer);
    Console.WriteLine("Year is : " + year);
    Console.WriteLine("Anthem is : " + anthem);
    
            }



    }
    class Program
    {
            static void Main(string[] args)
        {
        Console.WriteLine("Enter Title :");
        string title = Console.ReadLine();
        Console.WriteLine("Enter Composer :");
        string Composer = Console.ReadLine();
        Console.WriteLine("Enter Year :");
        string year = Console.ReadLine();
        bool isParsed = int.TryParse(year, out int result);
        Console.WriteLine("Enter Nation's Anthem :");
        string nationAnthem = Console.ReadLine();
        
        MusicalComposition q = new MusicalComposition(title, Composer, result);
        q.Display();
        MusicalComposition m = new NationAnthem(title, Composer, result, nationAnthem);
        m.Display();
        
        Console.ReadLine();
                                                     
        
                }
        }
}
