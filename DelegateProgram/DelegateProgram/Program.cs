﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DelegateProgram.Operations;

namespace DelegateProgram
{
    class Program
    {

        public static void Main(string[] args)
        {

            Operations.delmethod del1 = Operations.ProgramDel.show;
            Operations.delmethod del2 = new delmethod(ProgramDel.display);
            ProgramDel obj = new ProgramDel();
            delmethod del3 = obj.print;

            del1();
            del2();
            del3();
            Console.ReadLine();

        }

    }
    
        
    
}
    

