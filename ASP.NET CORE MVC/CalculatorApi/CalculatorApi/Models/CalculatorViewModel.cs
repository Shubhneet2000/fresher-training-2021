﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorApi.Models
{
    public class CalculatorViewModel
    {
        [Required]
        public int FirstNo { get; set; }
        [Required]
        public int SecondNo { get; set; }
        public float Result { get; set; }
    }
}
