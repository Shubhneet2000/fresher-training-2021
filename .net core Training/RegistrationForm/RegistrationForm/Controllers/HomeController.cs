﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RegistrationForm.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using RegistrationForm.Controllers;
using Microsoft.AspNetCore.Http;
using RegistrationForm;
using RegistrationForm.Repositories;

namespace RegistrationForm.Controllers
{
    public class HomeController : Controller
    {
        public  readonly ILogger<HomeController> _logger;
        private readonly IStudentRepository _studentRepository;

        public HomeController(ILogger<HomeController> logger, IStudentRepository studentRepository)
        {
            _logger = logger;
            _studentRepository = studentRepository;
        }

        
        
        public IActionResult Registration()
        {
            return View();
        }
        public ActionResult ShowData()
        {
            var data = _studentRepository.GetStudents();
            return View(data);
        }
        [HttpPost]
        // Get Data using FormCollection and CheckBox Data into name1 array
        public IActionResult SubmitData(FormCollection form)
        {
            var FName = form["txt1"].ToString();
            var MName = form["txt2"].ToString();
            var LName = form["txt3"].ToString();
            var Email = form["txt4"].ToString();
            var Password = form["txt5"].ToString();
            var Mobile = form["txt7"].ToString();
            var Address = form["txt8"].ToString();
            var Education = form["hidden1"].ToString();
            var Gender = form["gender"].ToString();
            
            Student obj = new Student { fName = FName, mName = MName, lName = LName,  email = Email, password=Password,mobile=Mobile,address=Address,education=Education, gender = Gender};
            //Store Employee Object Into TempData and Pass it
            TempData["Data"] = obj;
            return RedirectToAction("ShowData");
        }
        public IActionResult Site()
        {
            return View();
        } 
       /* public ActionResult ShowData()
        {
            if (TempData["Data"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Registration");
            }
        }*/
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
