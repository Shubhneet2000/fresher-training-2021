﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WTask.Models
{
    public class EmployeeModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int Mobile { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }

    }
}
