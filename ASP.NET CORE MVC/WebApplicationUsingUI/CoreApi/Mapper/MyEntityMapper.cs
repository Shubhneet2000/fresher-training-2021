﻿using AutoMapper;
using CoreApi.Models;
using Microsoft.Azure.Amqp.Framing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApi.Mapper
{
    public class MyEntityMapper : Profile
    {
        public MyEntityMapper()
        {
            CreateMap<Data.Model.Employee, EmployeeModel>();
            CreateMap<EmployeeModel, Data.Model.Employee>();



        }
    }
}
