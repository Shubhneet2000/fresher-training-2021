﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_may_18
{
    

    public class Sphere
    {
        public double r, x,y, h,surface_area, volume;
        public double PI = 3.14;
        public virtual void Volume(double r)
        {
            volume = (4.0 / 3) * PI * r * r * r;
            Console.WriteLine("Volume of sphere is : "+ volume);
        }
        public virtual void Surface_area(double r)
        {
            surface_area = 4 * PI * r * r;
            Console.WriteLine("Surface area of sphere is : " + surface_area);
        }

    }
    public class Cube : Sphere
    {
        public override void Volume(double x)
        {
            volume = x * x * x;
            Console.WriteLine("Volume of cube is : " + volume);

        }
        public override void Surface_area(double x)
        {
            surface_area = 6 * x * x;
            Console.WriteLine("Surface area of cube is : " + surface_area);
        }

    }
    public class Cylinder : Cube
    {
        public override void Volume(double y)
        {
            try
            {
                Console.WriteLine("Enter height of Cylinder");
                double h = Convert.ToDouble(Console.ReadLine());
            }
            catch(Exception)
            {
                Console.WriteLine("Do not enter null value");
                Console.WriteLine("Enter height of Cylinder");
                double h = Convert.ToDouble(Console.ReadLine());
            }
            volume = PI * y * y * h;
            Console.WriteLine("Volume of cylinder is : " + volume);
        }
        public override void Surface_area(double y)
        {
            
            surface_area = 2 * PI * y*(y+h);
            Console.WriteLine("Surface area of cylinder is : " + surface_area);
        }

    }
    class Program
    {


        static void Main(string[] args)
        {
            try
            {
                string condition;
                do
                {

                    Console.WriteLine("Select Shape");
                    Console.WriteLine("1. Sphere");
                    Console.WriteLine("2. Cube");
                    Console.WriteLine("3. Cylinder");

                    double value1 = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine();
                    Sphere obj1 = new Sphere();
                    Cube obj2 = new Cube();
                    Cylinder obj3 = new Cylinder();


                    switch (value1)
                    {
                        case 1:
                            try
                            {
                                Console.WriteLine("Enter radius of sphere");
                                double r = Convert.ToDouble(Console.ReadLine());
                                obj1.Volume(r);
                                obj1.Surface_area(r);
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                Console.WriteLine("Enter radius of sphere");
                                double r = Convert.ToDouble(Console.ReadLine());
                                obj1.Volume(r);
                                obj1.Surface_area(r);
                            }
                            break;
                        case 2:
                            try
                            {
                                Console.WriteLine("Enter side of Cube");
                                double x = Convert.ToDouble(Console.ReadLine());
                                obj2.Volume(x);
                                obj2.Surface_area(x);
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                Console.WriteLine("Enter side of Cube");
                                double x = Convert.ToDouble(Console.ReadLine());
                                obj2.Volume(x);
                                obj2.Surface_area(x);
                            }
                            break;
                        case 3:
                            try
                            {
                                Console.WriteLine("Enter radius of Cylinder");
                                double y = Convert.ToDouble(Console.ReadLine());
                                obj3.Volume(y);
                                obj3.Surface_area(y);
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                Console.WriteLine("Enter radius of Cylinder");
                                double y = Convert.ToDouble(Console.ReadLine());
                                obj3.Volume(y);
                                obj3.Surface_area(y);
                            }

                            break;

                    }
                    Console.WriteLine("Do You Want select shapes Again...");
                    condition = Console.ReadLine();

                } while (condition == "yes" || condition == "Yes");
                Console.ReadLine();
            }
            catch(Exception)
            {
                Console.WriteLine("please select option correctly");
                string condition;
                do
                {

                    Console.WriteLine("Select Shape");
                    Console.WriteLine("1. Sphere");
                    Console.WriteLine("2. Cube");
                    Console.WriteLine("3. Cylinder");

                    double value1 = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine();
                    Sphere obj1 = new Sphere();
                    Cube obj2 = new Cube();
                    Cylinder obj3 = new Cylinder();


                    switch (value1)
                    {
                        case 1:
                            try
                            {
                                Console.WriteLine("Enter radius of sphere");
                                double r = Convert.ToDouble(Console.ReadLine());
                                obj1.Volume(r);
                                obj1.Surface_area(r);
                            }
                            catch(Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                Console.WriteLine("Enter radius of sphere");
                                double r = Convert.ToDouble(Console.ReadLine());
                                obj1.Volume(r);
                                obj1.Surface_area(r);
                            }
                            break;
                        case 2:
                            try
                            {
                                Console.WriteLine("Enter side of Cube");
                                double x = Convert.ToDouble(Console.ReadLine());
                                obj2.Volume(x);
                                obj2.Surface_area(x);
                            }
                            catch(Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                Console.WriteLine("Enter side of Cube");
                                double x = Convert.ToDouble(Console.ReadLine());
                                obj2.Volume(x);
                                obj2.Surface_area(x);
                            }
                            break;
                        case 3:
                            try
                            {
                                Console.WriteLine("Enter radius of Cylinder");
                                double y = Convert.ToDouble(Console.ReadLine());
                                obj3.Volume(y);
                                obj3.Surface_area(y);
                            }
                            catch(Exception)
                            {
                                Console.WriteLine("Do not enter null value");
                                Console.WriteLine("Enter radius of Cylinder");
                                double y = Convert.ToDouble(Console.ReadLine());
                                obj3.Volume(y);
                                obj3.Surface_area(y);
                            }

                            break;

                    }
                    Console.WriteLine("Do You Want select shapes Again...");
                    condition = Console.ReadLine();

                } while (condition == "yes" || condition == "Yes");
                Console.ReadLine();
            }
        }
    }
}
