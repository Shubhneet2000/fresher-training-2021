﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_May_17
{
    class Program
    {
        float Add, Sub, Mul,Div;
        
        public void Addition(float Number1, float Number2)
        {
            Add = Number1 + Number2;
            Console.WriteLine("Addition of Two number is:" + Add);

        }
        public void Subtract(float Number1, float Number2)
        {
            Sub = Number1 - Number2;
            Console.WriteLine("Subtraction of Two number is:" + Sub);

        }
        public void Multiplication(float Number1, float Number2)
        {
            Mul = Number1 * Number2;
            Console.WriteLine("Multiplication of Two number is:" + Mul);

        }
        public void Division(float Number1, float Number2)
        {
            try
            {
                Div = Number1 / Number2;
                Console.WriteLine("Divison of Two number is:" + Div);
            }
            catch(DivideByZeroException )
            {
                
                Console.WriteLine("Division of {0} by zero.", Number1);

                
            }

        }
        public static void Main(string[] args)
        {
            try
            {
                int Number1, Number2;
                Console.WriteLine("please enter the Number1");
                Number1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("please enter the Number2");
                Number2 = Convert.ToInt32(Console.ReadLine());
                Program p = new Program();
                p.Addition(Number1, Number2);
                p.Subtract(Number1, Number2);
                p.Multiplication(Number1, Number2);
                p.Division(Number1, Number2);
                Console.ReadKey();
            }
            catch(Exception )
            {
                //Console.WriteLine(e);
                float Number1, Number2;
                Console.WriteLine("please enter the Number1");
                Number1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("please enter the Number2");
                Number2 = Convert.ToInt32(Console.ReadLine());
                Program p = new Program();
                p.Addition(Number1, Number2);
                p.Subtract(Number1, Number2);
                p.Multiplication(Number1, Number2);
                p.Division(Number1, Number2);
                Console.ReadKey();
            }


        }
    }
}
